#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <climits>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    Board * board;
    int ply;
    Side side_name;
    Move *doMove(Move *opponentsMove, int msLeft);
	std::vector<Move*> getValidMoves(Side side);
	Move* getHighestScoreMove();
	Move* minimax(int n);
	int minimax(Board* current_board, int n);
	int modify(Move * move, int score);
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
