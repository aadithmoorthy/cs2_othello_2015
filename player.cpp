#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) 
{
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
	board = new Board();
	side_name = side;
	ply = 4;
}

/*
 * Destructor for the player.
 */
Player::~Player() 
{
	delete board;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) 
{
    board->doMove(opponentsMove, (side_name == BLACK) ? WHITE : BLACK);
    
    //For minimax
    Move* move = minimax(ply);
    if(board->onBoard(move->getX(), move->getY()))
    { 
		board->doMove(move, side_name);
		return move;
	}
	
	//Resort to random if needed
    std::vector<Move*> moves = board->getValidMoves(side_name);
    if(moves.size() > 0)
    {
		Move * move = moves[rand() % int(moves.size())];
		board->doMove(move, side_name);
		return move;
	}
    return NULL;
    
    //For just heuristic
    /**Move* move = getHighestScoreMove();
    if(board->onBoard(move->getX(), move->getY()))
    { 
		board->doMove(move, side_name);
		return move;
	}
    
    return NULL;*/
    
    //For simple random
    /**std::vector<Move*> moves = board->getValidMoves(side_name);
    if(moves.size() > 0)
    {
		Move * move = moves[rand() % int(moves.size())];
		board->doMove(move, side_name);
		return move;
	}
    return NULL;*/
}

/**
 * @brief 2 ply minimax
 */
Move* Player::minimax(int n)
{
	int max = INT_MIN;
	int mini;
	Move* move = new Move(8,8);
	std::vector<Move*> ply1 = board->getValidMoves(side_name);
	
	Board** boards_ply1 = new Board*[ply1.size()];
	Side other = (side_name == BLACK) ? WHITE : BLACK;
	
	int score = 0;
	
	//ply 1
	for(unsigned int i = 0; i < ply1.size(); i++)
	{
		mini = INT_MAX;
		boards_ply1[i] = board->copy();
		boards_ply1[i]->doMove(ply1[i], side_name);
		std::vector<Move*> ply2 = boards_ply1[i]->getValidMoves(other);
		Board** boards_ply2 = new Board*[ply2.size()];
		//ply 2
		for(unsigned int j = 0; j < ply2.size(); j++)
		{
			boards_ply2[j] = boards_ply1[i]->copy();
			boards_ply2[j]->doMove(ply2[j], other);
			
			
			if(n - 2 > 1)
			{
				score = minimax(boards_ply2[j], n - 2);
				
				if(score > INT_MIN && score < mini)
				{
					mini = score;
				}
				else
				{
					score = boards_ply2[j]->count(side_name) - boards_ply2[j]->count(other);
					if(!testingMinimax)
					{
						//Take into account corners etc.
						score = modify(ply1[i], score);
					}
				
					if(score < mini)
					{
						mini = score;
					}
				}
			}
			else
			{
				score = boards_ply2[j]->count(side_name) - boards_ply2[j]->count(other);
				if(!testingMinimax)
				{
					//Take into account corners etc.
					score = modify(ply1[i], score);
				}
			
				if(score < mini)
				{
					mini = score;
				}
			}
		}
		
		if(mini > max)
		{
			max = mini;
			
			move = ply1[i];
		}
		
		score = 0;
	}
	
	return move;
	
}

int Player::minimax(Board* current_board, int n)
{
	int max = INT_MIN;
	int mini;
	
	std::vector<Move*> ply1 = current_board->getValidMoves(side_name);
	
	Board** boards_ply1 = new Board*[ply1.size()];
	Side other = (side_name == BLACK) ? WHITE : BLACK;
	
	int score = 0;
	
	//ply1
	for(unsigned int i = 0; i < ply1.size(); i++)
	{
		
		mini = INT_MAX;
		
		boards_ply1[i] = current_board->copy();
		
		boards_ply1[i]->doMove(ply1[i], side_name);
		
		std::vector<Move*> ply2 = boards_ply1[i]->getValidMoves(other);
		
		Board** boards_ply2 = new Board*[ply2.size()];
		
		//ply 2
		for(unsigned int j = 0; j < ply2.size(); j++)
		{
			
			boards_ply2[j] = boards_ply1[i]->copy();
			
			boards_ply2[j]->doMove(ply2[j], other);
			
			
			
			if(n - 2 > 1)
			{
				score = minimax(boards_ply2[j], n - 2);
				if(score > INT_MIN && score < mini)
				{
					mini = score;
				}
				else
				{
					score = boards_ply2[j]->count(side_name) - boards_ply2[j]->count(other);
					if(!testingMinimax)
					{
						//Take into account corners etc.
						score = modify(ply1[i], score);
					}
				
					if(score < mini)
					{
						mini = score;
					}
				}
			}
			else
			{
				score = boards_ply2[j]->count(side_name) - boards_ply2[j]->count(other);
				
				if(!testingMinimax)
				{
					//Take into account corners etc.
					score = modify(ply1[i], score);
				}
				
				if(score < mini)
				{
					mini = score;
				}
				
			}
			
		}
		
		if(mini > max)
		{
			max = mini;
		}
		
		score = 0;
		
	}
	
	return max;
}

/**
 * @brief modify score for corners, edges etc.
 */
int Player::modify(Move * move, int score)
{
	
	int x = move->getX();
	int y = move->getY();

    if (((x == 0) || (x == 7)) && ((y == 0) || (y == 7))) {
      score += 50;
    }

    if ((x == 0) || (x == 7) || (y == 0) || (y == 7)) {
      score += 10;
    }

    if ((x == 1) || (x == 6) || (y == 1) || (y == 6)) 
    {
	  score--;

	  if ((x == 1) && (y == 1) && (!board->get(side_name, 0, 0)))
		score -= 10;
	  if ((x == 1) && (y == 6) && (!board->get(side_name, 0, 7)))
		score -= 10;
	  if ((x == 6) && (y == 1) && (!board->get(side_name, 7, 0)))
		score -= 10;
	  if ((x == 6) && (y == 6) && (!board->get(side_name, 7, 7))) 
	  {
		score -= 10;
	  }
    }
    return score;
}


/**
 * @brief use simple heuristics to get move
 */
Move* Player::getHighestScoreMove()
{
	int high = INT_MIN;
	Move * move = new Move(8,8);
	int score = 0;
	Side other = (side_name == BLACK) ? WHITE : BLACK;
	for(int X = 0; X < 8; X++)
	{
		for(int Y = 0; Y < 8; Y++)
		{
			score = 0;
			if (!board->checkMove(X, Y, side_name)) continue;
			
			for (int dx = -1; dx <= 1; dx++) {
				for (int dy = -1; dy <= 1; dy++) {
					if (dy == 0 && dx == 0) continue;

					int x = X;
					int y = Y;
					do {
						x += dx;
						y += dy;
					} while (board->onBoard(x, y) && board->get(other, x, y));

					if (board->onBoard(x, y) && board->get(side_name, x, y)) {
						x = X;
						y = Y;
						x += dx;
						y += dy;
						while (board->onBoard(x, y) && board->get(other, x, y)) {
							score++;
							x += dx;
							y += dy;
						}
					}
				}
			}
			
			
			score = modify(new Move(X, Y), score);
			
			if(score > high)
			{
				high = score;
				move->setX(X);
				move->setY(Y);
			}
				
		}
	}
	
	
	
	return move;
	
}
