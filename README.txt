The main improvements I made to my AI to make it tournament worthy, were to make
it construct a minimax decision tree to a depth of 4 and updating the heuristic function.
The increased minimax tree depth enables the AI to maximize its outlook on future
moves to enable current decision making. The changed heuristic function will enable
better prediction of moves by promoting good moves and reducing the scores of 
stragtegically bad ones. I believe these two changes have truly made my AI tournament
worthy.
